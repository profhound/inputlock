unit MainWin;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  screenwin;

type

  { TMainForm }

  TMainForm = class(TForm)
    btnAbout: TButton;
    btnQuit: TButton;
    btnUnlockWithEsc: TButton;
    btnUnlockWithPass: TButton;
    cbShowLock: TCheckBox;
    imgLock: TImage;
    lePassphrase: TLabeledEdit;
    Panel1: TPanel;
    Panel2: TPanel;
    procedure btnAboutClick(Sender: TObject);
    procedure cbShowLockChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure UnlockWithEnterOrEscClick(Sender: TObject);
    procedure UnlockWithPassClick(Sender: TObject);
    procedure QuitClick(Sender: TObject);
    procedure imgLockClick(Sender: TObject);
    procedure BitcoinClick(Sender: TObject);
    procedure LitecoinClick(Sender: TObject);
    procedure WWWClick(Sender: TObject);
  private
    FScreenFrm: TScreenForm;
    FOldWidth: Integer;
  public

  end;

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

uses
  aboutwin,
  LCLType;

{ TMainForm }

procedure TMainForm.UnlockWithEnterOrEscClick(Sender: TObject);
begin
  Hide;
  FScreenFrm := TScreenForm.Create(Application, True, lePassphrase.Text);
  try
    FScreenFrm.ShowModal;
  finally
    FScreenFrm.Free;
    Show;
  end;
end;

procedure TMainForm.btnAboutClick(Sender: TObject);
begin
  AboutForm.ShowModal;
end;

procedure TMainForm.cbShowLockChange(Sender: TObject);
begin
  if cbShowLock.Checked then
  begin
    FOldWidth := Width;
    Width:= imgLock.Width + (imgLock.Left * 2) ;
  end else
  begin
    Width:= FOldWidth;
  end;

end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  //AboutForm.CreateMainForm(Sender); //Hack
  AboutForm.ShowingMainForm(Sender);
end;

procedure TMainForm.UnlockWithPassClick(Sender: TObject);
begin
  Hide;
  FScreenFrm := TScreenForm.Create(Application, False, lePassphrase.Text);
  try
    FScreenFrm.ShowModal;
  finally
    FScreenFrm.Free;
    Show;
  end;
end;

procedure TMainForm.QuitClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TMainForm.imgLockClick(Sender: TObject);
begin
  Application.MessageBox('To lock input, select passphrase or enter/esc button.', 'Hint', MB_ICONINFORMATION)
end;

procedure TMainForm.BitcoinClick(Sender: TObject);
begin
  AboutForm.BitcoinClick(Sender);
end;

procedure TMainForm.LitecoinClick(Sender: TObject);
begin
  AboutForm.LitecoinClick(Sender);
end;

procedure TMainForm.WWWClick(Sender: TObject);
begin
  AboutForm.WWWClick(Sender);
end;

end.

