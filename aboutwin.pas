unit aboutwin;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls;

const
  constversion = '20200410.NOGIT';

type

  { TAboutForm }

  TAboutForm = class(TForm)
    btnClose: TButton;
    btnUpdate: TButton;
    chbCheckForUpdate: TCheckBox;
    chbSaveIniFile: TCheckBox;
    cmbLocalisation: TComboBox;
    Image4: TImage;
    imgBtc: TImage;
    imgDoge: TImage;
    imgLogo: TImage;
    imgLtc: TImage;
    lblBtc: TLabel;
    lblDoge: TLabel;
    lblEmail: TLabel;
    lblLocal: TLabel;
    lblLtc: TLabel;
    lblSupportUs: TLabel;
    lblUrl: TLabel;
    memoVersion: TMemo;
    procedure btnCloseClick(Sender: TObject);
    procedure btnUpdateClick(Sender: TObject);
    procedure DogecoinClick(Sender: TObject);
    procedure BitcoinClick(Sender: TObject);
    procedure LitecoinClick(Sender: TObject);
    procedure WWWClick(Sender: TObject);
  private
    fMustUpgrade: Boolean;
    function CheckOnlineVersion(aSilent: Boolean): String;
    function DownloadFile(Url, FileName: String): Boolean;
    procedure OnData(Sender: TObject; const ContentLength, CurrentPos: Int64);
    function RunFile(FileName: string): Boolean;
    procedure SetMustUpgrade(AValue: Boolean);

  public
    procedure CreateMainForm(Sender: TObject);
    procedure ShowingMainForm(Sender: TObject);
    property MustUpgrade: Boolean read fMustUpgrade write SetMustUpgrade;
  end;

var
  AboutForm: TAboutForm;
  ConfigFile: string = 'inputlock.ini';
  Config: TStringList;

implementation

{$R *.lfm}


uses
  MainWin,
  lclintf,
  lcltype,
  fphttpclient,
  md5,
  ActiveX,
  ComObj,
  Variants,
  LazUTF8,
  fpopenssl,
  openssl,
  process,
  strutils,
  fileutil,
  opensslsockets;

{ TAboutForm }

procedure TAboutForm.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TAboutForm.btnUpdateClick(Sender: TObject);
const
  exeUrl = 'https://gitlab.com/profhound/inputlock/-/jobs/artifacts/master/raw/inputlock.exe?job=release';
  exeFilename = 'inputlock.new.exe';
var
  exeCurrentFilename : string;
begin
  if DownloadFile(exeUrl, exeFilename) then
  begin
    if RunFile(exeFilename) then
    begin
      Application.Terminate;
    end else
    begin
      ShowMessage(Format('Could not start "%s", check file permission.', [exeFilename]))
    end;

  end else
  begin
    ShowMessage(Format('Could not download "%s", check your internet connection.', [exeUrl]))
  end;
end;

procedure TAboutForm.DogecoinClick(Sender: TObject);
begin
  OpenUrl('dogecoin:xxx')
end;

procedure TAboutForm.BitcoinClick(Sender: TObject);
begin
  OpenUrl('bitcoin:395AkXQv4TMW2DyLwMZqX7LDBjLWB7kCia')
end;

procedure TAboutForm.LitecoinClick(Sender: TObject);
begin
  OpenUrl('litecoin:MVouZNe12WYeAv5yaLjpLf3J4muo9VwzJ6')
end;

procedure TAboutForm.WWWClick(Sender: TObject);
begin
  OpenUrl('https://gitlab.com/profhound/inputlock')
end;

function TAboutForm.CheckOnlineVersion(aSilent: Boolean): String;
const
  UNKNOWN = '0.unknown';
var
  urls: array[0..1] of string = (
    'http://planeteer.mooo.com/profhound/inputlock/version.txt',
    'http://gitlab.com/profhound/inputlock/-/raw/master/version.txt'
    );
  telemetry, uid: string; // current version + locale + user id

  function GetSerialNumber(var ASerial: WideString): Boolean;
    const
      FlagForwardOnly = $00000020;
      WMIClass = 'Win32_PhysicalMedia';
      WMIProperty = 'SerialNumber';
    var
      SWbemLocator, WMIService: Variant;
      WbemObjectSet, WbemObject: OleVariant;
      EnumVarinat: IEnumvariant;
      Value: LongWord;
      SQL: WideString;
    begin
      Result := False;
      SWbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
      WMIService := SWbemLocator.ConnectServer('localhost', 'root\CIMV2', '', '');
      SQL := WideString(Format('SELECT %s FROM %s', [WMIProperty, WMIClass]));
      try
        WbemObjectSet := WMIService.ExecQuery(SQL, 'WQL', FlagForwardOnly);
        EnumVarinat := IUnknown(WbemObjectSet._NewEnum) as IEnumVariant;
        if EnumVarinat.Next(1, WbemObject, Value) = 0 then
        begin
          if not VarIsNull(WbemObject.Properties_.Item(WMIProperty).Value) then
            ASerial := WbemObject.Properties_.Item(WMIProperty).Value;
        end;
        WbemObject := Unassigned;
        Result:= True;
      except
        //on E: Exception do
          //ShowMessage('Cannot get serial number: ' + sLineBreak + E.Message);
      end;
    end;

  function GetUid: String;
  var
    Serial: WideString;
    gur: Integer;
    gu: TGuid;
  begin
    Serial := '';
    if GetSerialNumber(Serial) then
      Result := MD5Print(MD5String(Serial))
    else
    begin
      gur := CreateGUID(gu);
      Result := MD5Print(MD5String(GUIDToString(gu)));
    end;
    Config.Values['uid'] := Result;
  end;
  function GetFromUrl(aUrl:String): string;
  begin
    uid := Config.Values['uid'];
    if uid = '' then uid := GetUid;
    telemetry := Format('v=%s&l=%s&i=%s', [Config.Values['version'], Config.Values['locale'], uid]);
    try
      Result := TFPHttpClient.SimpleGet(aUrl + '?' + telemetry);
    except
      on E: Exception do
      begin
        Result := UNKNOWN;
      end;
    end;
  end;
begin
  Result := GetFromUrl(urls[0]);
  if Result = UNKNOWN then GetFromUrl(urls[1]);
  if (Result = UNKNOWN) and (not aSilent) then
     Application.MessageBox('Could not check for update. Are you connected to the internet?', 'Error', MB_ICONWARNING);
end;

procedure TAboutForm.OnData(Sender: TObject; const ContentLength,
  CurrentPos: Int64);
var
  perc: Int64;
begin
  if ContentLength = 0 then
    perc := 0
  else
    perc := Round(CurrentPos / ContentLength * 100);
  memoVersion.Text := format('PLEASE WAIT FOR UPDATE TO DOWNLOAD!' + #10 + 'PROGRESS %d / 100 %',[ perc]);
  Application.ProcessMessages;
end;

function TAboutForm.DownloadFile(Url, FileName: String): Boolean;
{ https://wiki.lazarus.freepascal.org/fphttpclient }
var
  Client: TFPHttpClient;
  FS: TStream;
  memoText: string;
begin
  Result := False;
  { SSL initialization has to be done by hand here }
  InitSSLInterface;
  Client := TFPHttpClient.Create(nil);
  Client.OnDataReceived:= @OnData;
  FS := TFileStream.Create(Filename,fmCreate or fmOpenWrite);
  memoText := memoVersion.Text;
  try
    try
      { Allow redirections }
      Client.AllowRedirect := true;
      Client.Get(Url,FS);
    except
      on E: EHttpClient do
        writeln(E.Message)
      else
        raise;
    end;
  finally
    FS.Free;
    Client.Free;
    memoVersion.Text := memoText;
  end;
  { Test our file }
  if FileExists(Filename) then
    Result := True;
end;

function TAboutForm.RunFile(FileName: string): Boolean;
var
  Process: TProcess;
  I: Integer;
begin
  Result := False;
  Process := TProcess.Create(nil);
  try
    Process.InheritHandles := False;
    Process.Options := [];
    Process.ShowWindow := swoShow;

    // Copy default environment variables including DISPLAY variable for GUI application to work
    for I := 1 to GetEnvironmentVariableCount do
      Process.Environment.Add(GetEnvironmentString(I));

    Process.Executable := FileName;
    Process.Execute;
  finally
    Process.Free;
  end;
  Result := True;
end;

procedure TAboutForm.SetMustUpgrade(AValue: Boolean);
begin
  if fMustUpgrade=AValue then Exit;
  btnUpdate.Enabled:= AValue;
  Show;
  fMustUpgrade:=AValue;
end;

procedure TAboutForm.CreateMainForm(Sender: TObject);
const
  exeFilename = 'inputlock.exe';

var
  exeCurrentFilename, exeNewFilename: string;
begin
  exeCurrentFilename := paramstr(0);
  exeNewFilename := ExtractFilePath(exeCurrentFilename) + '/inputlock.new.exe';
  if IndexStr(exeCurrentFilename, ['new']) <> -1 then
  begin
    if FileExists(exeFilename) then
    begin
      if DeleteFile(exeCurrentFilename) then
      begin
        //pass
      end else
      begin
        ShowMessage(Format('Could not delete "%s", check file permission.', [exeCurrentFilename]));
        exit;
      end;
    end;
    if CopyFile(exeCurrentFilename, exeFilename) then
    begin
      if RunFile(exeFilename) then
        Application.Terminate;
    end else
    begin
      ShowMessage(Format('Could not copy "%s" to "%s", check file permission.', [exeCurrentFilename, exeFilename]))
    end;
  end;
  if FileExists(exeNewFilename) then
  begin
    if DeleteFile(exeNewFilename) then
    begin
      //pass
    end;
  end;
end;

procedure TAboutForm.ShowingMainForm(Sender: TObject);
var
  currentversion, ignoreversion, onlineversion: string;
  currentversions, onlineversions: TStringArray;
  i: integer;
begin
  try
     config := TStringList.Create();
     if FileExists(configfile) then
       config.LoadFromFile(configfile);
     currentversion := config.Values['version'].Trim;
     if currentversion = '' then
       currentversion := constversion;
     ignoreversion := config.Values['ignore_version'].Trim;
     onlineversion := CheckOnlineVersion(True).Trim;
     memoVersion.Text := Format(memoVersion.Text, [currentversion, onlineversion]);
     if ignoreversion = currentversion then
     begin
       memoVersion.lines[2] := Format('Ignoring Version(%s)', [ignoreversion]);
       memoVersion.lines[3] := '';
       exit;
     end;
     currentversions := currentversion.split(['.']);
     onlineversions := onlineversion.split(['.']);
     if onlineversions[0].ToInteger > currentversions[0].ToInteger then
     begin
       memoVersion.lines[2] := memoVersion.lines[3];
       memoVersion.lines[3] := '';
       MustUpgrade := True;

     end else
     begin
       memoVersion.lines[3] := '';
       MustUpgrade := False;
     end;
     for i := memoVersion.lines.Count - 1 downto 0 do
       if memoVersion.lines[i].Trim = '' then
         memoVersion.lines.Delete(i);
  finally
    //FreeAndNil(config);
  end;
end;

finalization
  Config.SaveToFile(ConfigFile);
end.

